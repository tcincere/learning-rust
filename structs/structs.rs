// Creating a struct with meaning
// Adding debug to print out debugging information.
#[derive(Debug)]
struct Rectangle {
    height: u32,
    width: u32,
}

fn main() {
    // Applying data to the fields of the struct.
    let rect = Rectangle {
        width: 50,
        height: 35,
    };

    // Getting the product of h * w.
    // Not taking ownership of the struct.
    // let area: u32 = area(&rect);
    // println!("Area = {:?}", area);

    println!("Rectangle: {:#?}", rect);
}

// Functions which takes a struct and returns an unsigned integer.
// Doesn't take ownership of the struct.
fn area(r: &Rectangle) -> u32 {
    r.width * r.height
}
