#[derive(Debug)]
struct Rectangle {
    width: u32,
    height: u32,
}

// Create a method; starting an implementation.
impl Rectangle {
    fn area(&self) -> u32 {
        self.width * self.height
    }
}

fn main() {
    let rect = Rectangle {
        height: 35,
        width: 55,
    };

    println!("Struct is: {:#?}", rect);

    let result: u32 = rect.area();
    println!("Result is: {}", result);
}
