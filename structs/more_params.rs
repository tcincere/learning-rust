#[derive(Debug)]
struct Rectangle {
    width: u32,
    height: u32,
}

// Create a method; starting an implementation.
impl Rectangle {
    // Method to calculate area of rectangle.
    fn area(&self) -> u32 {
        self.width * self.height
    }

    // Creating another function associated
    // with the struct 'Rectangle'.
    fn greater_than(&self, other: &Rectangle) -> bool {
        /*
            Reference to self as first parameter
            And add reference to other (other struct instances)
            Function returns a boolean (true or false).
        */

        // Checks if the first rectangle is bigger than the second (other)
        self.width > other.width && self.height > other.height
    }
}

fn main() {
    // First rectangle definition
    let rect = Rectangle {
        height: 35,
        width: 55,
    };

    // First rectangle definition
    let rect2 = Rectangle {
        height: 10,
        width: 15,
    };

    // First rectangle definition
    let rect3 = Rectangle {
        height: 60,
        width: 75,
    };

    // println!("Struct is: {:#?}", rect);
    // let result: u32 = rect.area();
    // println!("Result is: {}", result);

    println!("Is rect greater than rect2? {}", rect.greater_than(&rect2));
    println!("Is rect greater than rect3? {}", rect.greater_than(&rect3));
}
