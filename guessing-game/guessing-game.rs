use rand::Rng;
use std::cmp::Ordering;
use std::io;

fn create_secret_number() -> u32 {
    // Secret number
    let secret_number = rand::thread_rng().gen_range(1..=100);

    return secret_number;
}

fn main() {
    let secret_number: u32 = create_secret_number();
    println!("Secret number is: {}", secret_number);

    loop {
        println!("Input a number:");

        // Creating variable, asking for input
        let mut guess = String::new();

        io::stdin()
            .read_line(&mut guess)
            .expect("Failed to read line");

        // Convert to integer and print guess.
        let guess: u32 = guess.trim().parse().expect("Enter an integer!");
        println!("You guessed: {guess}");

        // Match guess
        match guess.cmp(&secret_number) {
            Ordering::Less => println!("[!] Number is too small."),
            Ordering::Greater => println!("[!] Number is too big."),
            Ordering::Equal => {
                println!(":) You win.");
                break;
            }
        }
    }
}
