use std::io;

fn main() {
    let a = [1, 2, 3, 4, 5];

    println!("Enter index: ");

    let mut index = String::new();

    io::stdin()
        .read_line(&mut index)
        .expect("Failed to read line");

    let index: usize = index.trim().parse().expect("Index is not a number.");

    if index > a.len() {
        println!("Value entered is greater than array length!");
    } else {
        println!("Value of element at index {index} is {}.", a[index]);
    }
}
