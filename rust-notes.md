## Chapter IV: Ownership
collapsed:: true
	- **Ownership rules: **
		- Each value in Rust has a variable called its owner.
		  logseq.order-list-type:: number
		- There may only be one owner at a time.
		  logseq.order-list-type:: number
		- The value is dropped when the owner goes out of scope.
		  logseq.order-list-type:: number
	- For example:
		- ```rust
		  {
		  	let s: = "test";
		  }
		  ```
		- Here the variable would be valid only within the scope; it is fixed-size string literal. This means we cannot modify it. In order to modify it, we need to convert it into a string type.
		  ```rust
		  {
		  	let s: = String::from("test");
		  }
		  ```
		  Here Rust allocates memory for this on the heap and destroys it automatically when it goes out of scope.
		-
		- ### Cloning
			- Unlike other programming languages, Rust does not allow cloning from one variable to another. For example:
			  ```rust
			  fn main()
			  {
			  	let s1: = String::from("test");
			    	let s2: = s1; 
			  }
			  ```
			  This code instead moves the data held in "s1" to "s2". Therefore in order to create a clone or copy, use the built-in method:
			  ```rust
			  fn main()
			  {
			  	let s1: = String::from("test");
			    	let s2: = s1.clone(); 
			  }
			  ```
			  **Note** Integers, booleans and characters can be copied, not moved without the use of the `.clone()` method
			-
		- ### References
			- References allow you to borrow from a variable without taking ownership. This uses a pointer to the variable via the `&`. This points to the memory address of the variable. References are immutable by default. We can mutate references without taking ownership. For example:
			  ```rust
			  fn main() 
			  {
			  	let mut s1: = String::from("test");
			    
			    	# Add a mutable to function.
			    	modify_string(&mut s1);
			  }
			  
			  fn modify_string(some_string: &mut String)
			  {
			    	# We can append to the string without taking ownership.
			    	some_string.push_str("testing again");
			  }
			  ```
			- ### Reference Rules
				- At any given time, you may have either one mutable reference or n number of immutable references.
				  logseq.order-list-type:: number
				- References must always be valid; that is they must point to valid data.
				  logseq.order-list-type:: number
- ---
- ## Chapter V: Structs
  collapsed:: true
	- **Structs** allow you to group together variables of different data types into a meaningful group. They are similar to tuples in the sense that they hold multiple related values. To define a struct, we enter the keyword `struct` and name the entire struct. Then we define the names and types of the pieces of data, which we call fields.
	  collapsed:: true
		- **For example:**
		  ```rust
		  struct User
		  {
		    	active: bool,
		    	username: String,
		    	email: String,
		  }
		  ```
		  Then, we can "fill in" this template with actual data.
		  ```rust
		  fn main()
		  {
		  	let user1 = User {
		      	active: true,
		        	username: String:from("example_user"),
		        	email: String::from("user@example.com")
		    	};
		  }
		  ```
		  We can also access data from a certain field using dot notation. Furthermore, if the instance is marked as mutable, we can then also modify the contents of each field, if necessary. 
		  ```rust
		  fn main()
		  {
		  	let mut user1 = User {
		      	active: true,
		        	username: String:from("example_user"),
		        	email: String::from("user@example.com")
		    	};
		    
		    	user1.username = String::from("changeemail@example.com");
		  }
		  ```
		  We can create another instance of the struct with similiar field data. However, we didn't copy the data from the `user1` instance. Instead, we can use the struct update syntax `..` to do this.
		  ```rust
		  fn main()
		  {
		  	let mut user1 = User {
		      	active: true,
		        	username: String:from("example_user"),
		        	email: String::from("user@example.com")
		    	};
		    
		    	let mut user2 = User {
		        	active: false,
		        	..user2
		    	};
		  }
		  ```
		  The `..` tells Rust that all the data fields will be inherited from `user1` while modifiying the `active` data field to false.
	- **Tuple Structs**
	  collapsed:: true
		- These types of structs simply have types of their fields, but with no name associated with it. We define a tuple struct like so:
		  ```rust
		  struct Colour(i32, i32, i32);
		  struct Point(i32, i32, i32);
		  
		  fn main() {
		      let black = Colour(0, 0, 0);
		      let rgb = Colour(255, 255, 255);
		  }
		  
		  ```
		  Here, each struct defined is its own type despite the fields having the same type.
	- **Unit-Like Structs**
	  collapsed:: true
		- These are structs which don't have any fields. For example when a trait must be implemented but there is no data you want to store in the type itself. An example:
		  ```rust
		  struct AlwaysEqual;
		  
		  fn main() {
		   	let subject = AlwaysEqual;
		  }
		  ```
		  Here an instance of the struct is stored in the `subject` variable.
	- **Method Syntax**
		- Unlike functions methods are defined within the context of a struct (or an enum or a trait object). To define a function within the context of the struct object, we start an implementation (`impl`) block for the object.  The first argument when calling a method is always `&self`; the instance the method is being called on. 
		  
		  In addition, when calling a method, you need not use the `->` syntax. Rust uses automatic referencing and referencing. For example: 
		  ```rust
		  p1.distance(&p2);
		  (&p1).distance(&p2);
		  ```
		  
		  An example of refactoring the code using the `impl` keyword would be as so:
		  ```rust
		  #[derive(Debug)]
		  struct Rectangle {
		      width: u32,
		      height: u32,
		  }
		  
		  // Create a method; starting an implementation.
		  impl Rectangle {
		      fn area(&self) -> u32 {
		          self.width * self.height
		      }
		  }
		  
		  fn main() {
		      let rect = Rectangle {
		          height: 35,
		          width: 55,
		      };
		  
		      println!("Struct is: {:#?}", rect);
		  
		      let result: u32 = rect.area();
		      println!("Result is: {}", result);
		  }
		  ```
		- ### Multiple Methods
			- Each struct can also have multiple `impl` blocks; written in a separate or the same block. For example:
			  ```rust
			  #[derive(Debug)]
			  struct Rectangle {
			      width: u32,
			      height: u32,
			  }
			  
			  // Create a method; starting an implementation.
			  impl Rectangle {
			      // Method to calculate area of rectangle.
			      fn area(&self) -> u32 {
			          self.width * self.height
			      }
			  
			      // Creating another function associated
			      // with the struct 'Rectangle'.
			      fn greater_than(&self, other: &Rectangle) -> bool {
			          /*
			              Reference to self as first parameter
			              And add reference to other (other struct instances)
			              Function returns a boolean (true or false).
			          */
			  
			          // Checks if the first rectangle is bigger than the second (other)
			          self.width > other.width && self.height > other.height
			      }
			  }
			  
			  fn main() {
			      // First rectangle definition
			      let rect = Rectangle {
			          height: 35,
			          width: 55,
			      };
			  
			      // First rectangle definition
			      let rect2 = Rectangle {
			          height: 10,
			          width: 15,
			      };
			  
			      // First rectangle definition
			      let rect3 = Rectangle {
			          height: 60,
			          width: 75,
			      };
			  
			      println!("Is rect greater than rect2? {}", rect.greater_than(&rect2));
			      println!("Is rect greater than rect3? {}", rect.greater_than(&rect3));
			  }
			  ```
- ---
- ## Chapter VI: Enums
	- Enums allow a programmer to say a value could be a range or set of possible values. For example a shape could be a possible set of shapes such as triangle, rectangle and square. A use enum is called `Option` which expresses that a value can equal either something or nothing. We can take a look at the example of IP addresses where each address can either be a version 4 or version 6 address.
		- For example:
		  ```rust
		  enum IpAddrVersion {
		   	V4, 
		    	V6,
		  }
		  ```
		  An instance can be created from each of the two variants, like so:
		  ```rust
		  let four = IpAddrVersion::V4;
		  let six = IpAddrVersion:V6;
		  ```
		  Here however there is no data associated with the enum. We can set an associated type for the version. Moreover, enums allow you to have different types and amounts of associated data unlike structs. For example:
		  ```rust
		  enum IpAddrVersion {
		    	V4(u8, u8, u8, u8),
		    	V6(String),
		  }
		  
		  let local_ipv4 = IpAddrVersion::V4(127, 0, 0, 1);
		  let local_ipv6 = IpAddrVersion::V6(String::from("::1"));
		  ```
		  We can have a variety of types within an enum, similar to structs, yet all the definitions are grouped together. For example:
		  ```rust
		  enum Message {
		    	Quit,
		    	Move { x: i32, x: i32 },
		    	Write(String),
		    	ChangeColour(i32, i32, i32),
		  }
		  ```
		  This enum has four variants with various types.
	- ### The `Option` Enum
		- The `Option` type is used when a value could be something or nothing. Rust does not have null values. The problem arises when you attempt to use a null value as a not-null value. Rust however includes the ability to encode the value being present or absent. The enum option is defined in the standard library like so:
		  ```rust
		  enum Option<T> {
		    	None, 
		    	Some(T),
		  }
		  ```
		  Here <T> is a generic type parameter which means that `Some` variant of the `Option`is able to hold on to data of any type. Furthermore, an example show using such values to hold both number and string types.
		  ```rust
		  let a_number = Some(5);
		  let a_char = Some('e');
		  
		  let absent_number: Option<i32> = None;
		  ```
		  Here the type of `a_number` is `Option<i32>` which is a different type. With `Some` value, there is obviously a value present. With a `None` value is means the data is absent. Rust will only compile if the data type definitely has a value. If it is of the `Option` type you must handle if the value is null (or absent).
	- ### The `match` Control Flow
		- The `match` expression allows you to compare a value against  a set of patterns. With `match`, every outcome must be accounted for, or the program will not compile. For example:
		  ```rust
		  enum Coin {
		      Penny,
		      Nickel,
		      Dime,
		      Quarter,
		  }
		  
		  fn value_in_cents(coin: Coin) -> u8 {
		      match coin {
		          Coin::Penny => 1,
		          Coin::Nickel => 5,
		          Coin::Dime => 10,
		          Coin::Quarter => 25,
		      }
		  }
		  ```
- ## Output
  collapsed:: true
	- You can print output in multiple ways in Rust; you just have to be explicit. Using the `println!();` macro you can debugging information (such as a struct and its fields). However you need to include `#[derive(Debug)]` before the struct definition. For example:
	  ```rust
	  #[derive(Debug)]
	  struct Rectangle {
	   	height: u32,
	    	width: u32,
	  }
	  
	  fn main() {
	   	let rect = Rectangle {
	      	width: 50,
	        	height: 25,
	    	};
	  }
	  ```
	- ### Printing options
		- You can print the above struct showing all the fields. The output won't be the prettiest however.
		  ```rust
		  println!("The struct: {:?}", rect)
		  ```
		  However, for pretty print where the formatting will be kept to a readable standard:
		  ```rust
		  println!("The struct: {:#?}", rect)
		  ```
- ---