# Learning Rust 


## Description
Project created for learning rust. The repository will include .rs files containing practice code from each lesson
along with a markdown file including all notes documented during learning rust. 

## Note
The repository is based on the Rust book found [here](https://doc.rust-lang.org/stable/book/title-page.html)


## Licence
